package trakkr.entities

import javax.persistence.*

//Entity -> representation of a data in our table
//File name -> Pascal case

@Entity(name = "User")
@Table(name = "users")
data class User(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null,
    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    var email: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean = true,

    @Column(
        nullable = false,
        updatable = true,
        name = "userType"
    )
    var userType: String
)


